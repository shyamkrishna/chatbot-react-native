
  import React, { Component } from 'react';
  import { StyleSheet, Text, View ,SafeAreaView} from 'react-native';
  import { Bubble, GiftedChat } from 'react-native-gifted-chat';
  import  FlatListBasics  from './components/FlatListBasics';
 import { dialogFlowConfig } from './env';
 import { Dialogflow_V2 } from 'react-native-dialogflow';


  const  botAvatar = require('./assets/images/mascot.png');
  const BOT= {
    _id : 2,
    name : 'Mr.BOT',
    avatar : botAvatar
  }

    

  class App extends Component {

  state = {
    messages : [ {_id :1 , text : 'Hi, My Name is Mr.BOT' , createdAt : new Date() , user : BOT}],
    id : 1,
    name : 'shyam'
  };

  componentDidMount(){
     Dialogflow_V2.setConfiguration (
       dialogFlowConfig.client_email,
       dialogFlowConfig.private_key,
       Dialogflow_V2.LANG_ENGLISH_US,
       dialogFlowConfig.project_id
     )
  }
  onSend (messages = [] ) {

    this.setState( (prevState) => {
       return  { messages : GiftedChat.append(prevState.messages , messages) }
    });
    let message = messages[0].text;
    Dialogflow_V2.requestQuery(
         message ,
        (result ) => this.handlleGoogleResponse(result),
        (error) => console.log(error)
      
      )
}
 onQuickReply(quickReply) {

      this.setState( (prevState) => {
          messages : GiftedChat.append(prevState.messages , quickReply)
       });
      let message = quickReply[0].value;
      Dialogflow_V2.requestQuery(
        message ,
        (result ) => this.handlleGoogleResponse(result),
        (error) => console.log(error)
      
      )
}

handlleGoogleResponse(result){
   let text = result.queryResult.fulfillmentMessages[0].text;;
   let textMsg =text.text[0];
   this.sendBotResponse(textMsg);
}

sendBotResponse(text){
  let msg = {
    _id : this.state.messages.length +1 ,
    text,
    createdAt : new Date(),
    user : BOT
  }
  this.setState( prevState => {
     return { messages : GiftedChat.append(prevState.messages , [msg]) }
  })
}
renderBubble = (props) => {
    return ( <Bubble  
                {...props}  
                textStyle = {{ right : {color : 'white'} } }
                wrapperStyle = {{ 
                                    right : { backgroundColor : 'pink'}  ,
                                    left : {backgroundColor : 'yellow'}  ,
                               }}
                
                />)
}

  render(){
    return (
    
      <View style={styles.container}>
        <GiftedChat messages={this.state.messages} 
          renderBubble = {this.renderBubble}
          onSend = { (message) => this.onSend(message)} 
          onQuickReply = {(quickReply) => this.onQuickReply(quickReply)} 
          user = {{ _id : 1}}/>
          
      </View>
    
    );
  }
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff'
     
    }
  });

  export default App;
